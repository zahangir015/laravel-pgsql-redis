# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Laravel PostgreSQL Data listing with filter using redis cache  
* Version 1.0

### How do I get set up? ###

* Clone from -> git clone https://zahangir015@bitbucket.org/zahangir015/laravel-pgsql-redis.git
* Create postgresql database with name -  laravel_postgresql_redis
* Config .env and config/database.php file with PostgreSQL database credentials
* Run this command for database migration -  php artisan migrate
* Using this command pattern to export data in postgresql database -

  COPY persons(first_name, last_name, dob, email)

  FROM 'C:\sampledb\persons.csv'

  DELIMITER ','

  CSV HEADER;
* Config .env,config/database.php and config/cache.php file with Redis credentials
* Run the following command to Laravel development server - php artisan serve

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin - zahangirahammad@gmail.com
* Other community or team contact
