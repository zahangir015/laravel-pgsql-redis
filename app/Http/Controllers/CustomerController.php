<?php

namespace App\Http\Controllers;

use App\Http\Requests\CustomerFilterRequest;
use App\Models\Customer;
use App\Services\CachingService;
use App\Services\PaginationService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Cache;

class CustomerController extends Controller
{
    public function index(CustomerFilterRequest $request, PaginationService $paginationService, CachingService $cachingService)
    {

        $customerQuery = Customer::select(['id', 'name', 'email', 'phone', 'birth_date', 'country', 'ip_address', 'created_at']);
        $queryParamsManagerResponse = self::queryParamsManager($request, $customerQuery);
        $queryParams = ['year' => $queryParamsManagerResponse['year'], 'month' => $queryParamsManagerResponse['month']];

        if ($queryParamsManagerResponse['year'] || $queryParamsManagerResponse['month']) {
            $dataList = $cachingService->putAndGetData($queryParamsManagerResponse['redisKey'], $queryParamsManagerResponse['eloquentQuery'], 60);
            $customerList = $paginationService->generatePagination($request->input('page') ?? 1, 20, collect($dataList), $queryParams, '/dashboard');
        } else {
            $customerList = $customerQuery->orderBy('id')->paginate(20)->appends($queryParams);
        }

        return view('dashboard', [
            'customers' => $customerList,
        ]);
    }

    protected function queryParamsManager(CustomerFilterRequest $request, Builder $eloquentQuery): array
    {
        $year = ($request->has('year')) ? $request->input('year') : null;
        $month = ($request->has('month')) ? $request->input('month') : null;

        $redisKey = '';
        if ($year != null) {
            $eloquentQuery->whereYear("birth_date", $year);
            $redisKey .= 'year-' . $year;
        }
        if ($month != null) {
            $eloquentQuery->whereMonth("birth_date", $month);
            if ($year != null) {
                $redisKey .= ',';
            }
            $redisKey .= 'month-' . $month;
        }

        return compact('redisKey', 'eloquentQuery', 'year', 'month');
    }
}
