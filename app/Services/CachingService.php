<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

class CachingService
{
    function putAndGetData(string $redisKey, Builder $eloquentQuery, int $timeInSeconds)
    {
        $keys = array_filter(explode(',', $redisKey));
        if (count($keys) == 2) {
            if (Cache::has($keys[0])) {
                $month = explode('-', $keys[1])[1];
                return collect(Cache::get($keys[0]))
                    ->filter(function ($item, $key) use ($month) {
                        if ($month == date('m', strtotime($item->birth_date))) {
                            return $item;
                        }
                    })->all();
            }
        }

        if (!Cache::has($redisKey)) {
            Cache::put($redisKey, $eloquentQuery->orderBy('id')->get(), now()->addSeconds($timeInSeconds));
        }
        return Cache::get($redisKey);
    }
}
