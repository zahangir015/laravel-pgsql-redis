<?php

namespace App\Services;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;

class PaginationService
{
    function generatePagination(int $page, int $perPage, Collection $collection, array $queryParams, string $path): LengthAwarePaginator
    {
        $paginator = new LengthAwarePaginator(
            $collection->forPage($page, $perPage),
            $collection->count(),
            $perPage,
            $page,
            ['path' => url($path)]
        );

        return $paginator->appends($queryParams);
    }
}
