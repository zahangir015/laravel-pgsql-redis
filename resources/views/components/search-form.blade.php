<x-customer-filter-validation-errors class="mb-4" :errors="$errors"/>

<form method="GET" action="{{ route('dashboard') }}" class="pb-8">
    <div class="flex flex-wrap -mx-3 mb-6">
        <div class="w-full md:w-1/3 px-3 mb-6 md:mb-0">
            <x-label for="year" :value="__('Birth Year')"></x-label>
            <x-input id="year" class="block mt-1 w-full" type="number" placeholder="2021" name="year"
                     :value="old('year')" autofocus></x-input>
        </div>
        <div class="w-full md:w-1/3 px-3">
            <x-label for="year" :value="__('Birth Month')"></x-label>
            <x-input id="month" class="block mt-1 w-full" type="number" placeholder="12" name="month"
                     :value="old('month')" min="1" max="12" autofocus></x-input>
        </div>
        <div class="w-full md:w-1/3 px-3 mt-7">
            <x-button class="ml-3">
                {{ __('Filter') }}
            </x-button>
            <a href="{{ route('dashboard') }}"
               class="shadow bg-gray-500 hover:bg-gray-900 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded">Reset</a>
        </div>
    </div>
</form>
