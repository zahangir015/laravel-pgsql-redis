<button {{ $attributes->merge(['type' => 'submit', 'class' => 'shadow bg-yellow-500 hover:bg-green-700 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded']) }}>
    {{ $slot }}
</button>
