@props(['customers'])

<table class="border-collapse border border-green-800">
    <thead>
    <tr>
        <th class="border">ID</th>
        <th class="border">Full Name</th>
        <th class="border">Email</th>
        <th class="border">Phone</th>
        <th class="border">Birth Date</th>
        <th class="border">Country</th>
        <th class="border">IP Address</th>
        <th class="border">Created At</th>
    </tr>
    </thead>
    <tbody>
    @foreach($customers as $customer)
        <tr>
            <td class="border p-3">{{$customer->id}}</td>
            <td class="border p-3">{{$customer->name}}</td>
            <td class="border p-3">{{$customer->email}}</td>
            <td class="border p-3">{{$customer->phone}}</td>
            <td class="border p-3">{{$customer->birth_date}}</td>
            <td class="border p-3">{{$customer->country}}</td>
            <td class="border p-3">{{$customer->ip_address}}</td>
            <td class="border p-3">{{$customer->created_at}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
